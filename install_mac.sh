ssh_result=$(ssh -T git@github.com 2>&1)
if [[ ! $ssh_result =~ "You've successfully authenticated" ]]
then
  echo >&2 "There appears to be a problem with Github access:"
  echo >&2 "$ssh_result"
  exit 1
fi
#
if [[ -d "$project" ]]
then
  echo >&2 "Directory $project already exists remove or copy it before continuing"
  exit 1
fi
#
echo 'Asking for your root password:'
sudo echo >> /dev/null
mkdir 'uninstallers'
timezone=$(sudo systemsetup -gettimezone | sed 's/Time Zone: //') ; echo "Your timezone is $timezone"
  (
  git clone git@github.com:"$org"/"$project".git && (cd "$project" && git submodule update --remote --init && git submodule foreach 'git checkout master')
  ) &
  (
  vbox_link=$(curl 'https://www.virtualbox.org/wiki/Downloads' -L --silent | grep 'http.*virtualbox.*\.dmg' -oi | head -1)
  curl -L --output 'virtualbox.dmg' "$vbox_link"
    (
    hdiutil mount -noverify 'virtualbox.dmg'
    cp '/Volumes/VirtualBox/VirtualBox_Uninstall.tool' 'uninstallers/virtualbox.tool'
    sudo installer -package '/Volumes/VirtualBox/VirtualBox.pkg' -target '/Volumes/Macintosh HD'
    hdiutil unmount '/Volumes/VirtualBox'
    rm 'virtualbox.dmg'
    ) &
  vagrant_link=$(curl 'https://www.vagrantup.com/downloads.html' -L --silent | grep 'http.*vagrant.*\.dmg' -o | head -1)
  curl -L --output 'vagrant.dmg' "$vagrant_link"
    (
    hdiutil mount -noverify 'vagrant.dmg'
    cp '/Volumes/Vagrant/uninstall.tool' 'uninstallers/vagrant.tool'
    sudo installer -package '/Volumes/Vagrant/Vagrant.pkg' -target '/Volumes/Macintosh HD'
    hdiutil unmount '/Volumes/Vagrant'
    rm 'vagrant.dmg'
    vagrant plugin install vagrant-disksize
    ) &
  [[ -f 'Vagrantfile' ]] && mv 'Vagrantfile' 'Vagrantfile.old'
  curl -L -O 'https://bitbucket.org/beyondcompute/deploy/raw/master/Vagrantfile'
  wait
  vagrant box add 'ubuntu/xenial64'
  vagrant up
  ) &
wait
vagrant ssh -c "timezone='$timezone' ; $(curl -fsSL 'https://bitbucket.org/beyondcompute/deploy/raw/master/vm_config.sh')"
  vagrant reload &
if hash brew 2>/dev/null # brew installed
then
  brew tap homebrew/completions && brew install vagrant-completion && \
    echo "complete -F _vagrant vag" >> "$(brew --prefix)"/etc/bash_completion.d/vagrant && \
    grep -Fq "/etc/bash_completion.d/vagrant" ~/.bash_profile || printf "\nsource %s/etc/bash_completion.d/vagrant\n" "$(brew --prefix)" >> ~/.bash_profile && \
    source ~/.bash_profile
fi
wait
