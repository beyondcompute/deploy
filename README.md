### Prerequisites ###
1. You should have [Git](https://git-scm.com/downloads) installed
2. Check Github access by issuing `ssh -T git@github.com` if it says 'denied' then [set-up your ssh keys](https://help.github.com/articles/generating-ssh-keys/)

### Usage ###
Create a new directory and run:
```
sh -c "export org='xxx' ; export project='yyyy' ; $(curl -fsSL 'https://bitbucket.org/beyondcompute/deploy/raw/master/install_mac.sh')"
```
Where `xxx` is the name of your Github account, `yyyy` the name of your project.

### After that ###
You will have [Vagrant](https://docs.vagrantup.com/v2/getting-started/index.html) virtual machine running.

`vagrant halt` to shut it down

`vagrant up` to launch it again

`vagrant ssh` to get access to it (`vagrant ssh -c 'pwd'` � execute a command inside VM)

If you launch some service inside your virtual machine and you want to access it from your real one (for example, to manage mongodb) you should set up port forwarding, see [Vagrantfile](https://bitbucket.org/beyondcompute/deploy/src/Vagrantfile) (it is a ruby code):
```ruby
config.vm.network "forwarded_port", guest: 27017, host: 27017
```
Do `vagrant reload` after you've edited the [Vagrantfile](https://bitbucket.org/beyondcompute/deploy/src/Vagrantfile).

### ###
Your installation folder will be shared between your real and virtual machines. Inside VM it is visible as `/home/ubuntu/code` ( so your `%install-dir%/yyyy` will hopefully become `~/code/yyyy` ).

You want to edit your code and work with Git from your **local machine**.