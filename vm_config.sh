sudo chown ubuntu /etc/timezone && echo "$timezone" > /etc/timezone ; sudo chown root /etc/timezone
sudo apt update
sudo apt -y upgrade
sudo apt -y install python-pip
(
pip install docker-compose && \
  sudo curl -L https://raw.githubusercontent.com/docker/compose/$(docker-compose version --short)/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose && \
  sudo chown ubuntu /etc/bash_completion.d/docker-compose && echo "complete -F _docker_compose doc" >> /etc/bash_completion.d/docker-compose && \
  source /etc/bash_completion.d/docker-compose
) &
(
sudo apt -y install curl \
    linux-image-extra-$(uname -r) \
    linux-image-extra-virtual \
	apt-transport-https \
    ca-certificates
curl -fsSL https://yum.dockerproject.org/gpg | sudo apt-key add -
printf "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
echo 'Verifying fingerptint: #########################################'
apt-key fingerprint 58118E89F3A912897C070ADBF76221572C52609D
echo '################################################################'
printf "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
sudo add-apt-repository \
       "deb https://apt.dockerproject.org/repo/ \
       ubuntu-$(lsb_release -cs) \
       main" 
sudo apt update
sudo apt -y install docker-engine
) &
wait
sudo usermod -aG docker ubuntu
